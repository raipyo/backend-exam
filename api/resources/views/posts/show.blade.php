@extends('layouts.app')

@section('content')
	<a href="/home">Go back</a>
	<h1>{{ $post->title }}</h1>
	<div>
		{{ $post->body }}
	</div>
	<hr>
	<small>Written on {{ $post->created_at }}</small>
	<hr>
	<a href="/posts/{{ $post->id }}/edit">Edit</a>

	{!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'post']) !!}	
		{{ Form::hidden('_method','delete')}}
		{{ Form::submit('Delete')}}
	{!! Form::close() !!}
@endsection