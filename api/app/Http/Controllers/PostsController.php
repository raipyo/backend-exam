<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['guestcomments', 'guestdeletecomment']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        $posts = Post::all();

        foreach ($posts as $post) {
            $result['data'] = [
                'title'         => $post->title,
                'content'       => $post->content,
                'slug'          => $post->slug,
                'updated_at'    => $post->updated_at,
                'created_at'    => $post->created_at,
                'id'            => $post->id,
                'user_id'       => $post->user_id
            ];
        }
        return response()->json($result, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = [];
        $status = '';
        $post = Post::find($id);

        if ($post->delete()) {
            $result['status'] = 'record deleted successfully';
            $status = 200;
        } else {
            $result['status'] = 'no record found';
            $status = 500;
        }

        return response()->json($result, $status); 
    }

    /**
     * Find the comments from a post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comments($id)
    {
        $result = [];

        $comments = Comment::where('creator_id',$id)->get();

        foreach ($comments as $comment) {
            $result['data'] = [
                'body'              => $comment->body,
                'creator_id'        => $comment->creator_id,
                'creator_type'      => $comment->creator_type,
                'commentable_id'    => $comment->commentable_id,
                'commentable_type'  => $comment->commentable_type,
                'parent_id'         => $comment->parent_id,
                '_lft'              => $comment->_lft,
                '_rgt'              => $comment->_rgt,
                'updated_at'        => $comment->updated_at,
                'created_at'        => $comment->created_at
            ];
        }
        return response()->json($result, 200); 
    }

    /**
     * Delete the comment from a post.
     *
     * @param  int  $id
     * @param  int  $commentId
     * @return \Illuminate\Http\Response
     */
    public function deletecomment($id, $commentId)
    {
        $result = [];
        $status = '';

        $comment = Comment::where('creator_id',$id)->where('commentable_id',$commentId);

        if ($comment->delete()) {
            $result['status'] = 'record deleted successfully';
            $status = 200;
        } else {
            $result['status'] = 'no record found';
            $status = 500;
        }
        return response()->json($result, $status); 
    }
    
    /**
     * Find the comments from a post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function guestcomments($id)
    {
        $result = [];

        $comments = Comment::where('creator_id',$id)->get();

        foreach ($comments as $comment) {
            $result['data'] = [
                'body'              => $comment->body,
                'creator_id'        => $comment->creator_id,
                'creator_type'      => $comment->creator_type,
                'commentable_id'    => $comment->commentable_id,
                'commentable_type'  => $comment->commentable_type,
                'parent_id'         => $comment->parent_id,
                '_lft'              => $comment->_lft,
                '_rgt'              => $comment->_rgt,
                'updated_at'        => $comment->updated_at,
                'created_at'        => $comment->created_at
            ];
        }
        return response()->json($result, 200); 
    }

    /**
     * Delete the comment from a post.
     *
     * @param  int  $id
     * @param  int  $commentId
     * @return \Illuminate\Http\Response
     */
    public function guestdeletecomment($id, $commentId)
    {
        $result = [];
        $status = '';

        $comment = Comment::where('creator_id',$id)->where('commentable_id',$commentId);

        if ($comment->delete()) {
            $result['status'] = 'record deleted successfully';
            $status = 200;
        } else {
            $result['status'] = 'no record found';
            $status = 500;
        }
        return response()->json($result, $status); 
    }
}
