@extends('layouts.app')

@section('content')
	<h1>Edit Post</h1>

	{!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'post']) !!}
		<div>
			{{ Form::label('title', 'Title') }}
			{{ Form::text('title', $post->title, ['placeholder' => 'Title']) }}
		</div>
		<div>
			{{ Form::label('body', 'Body') }}
			{{ Form::textarea('body', $post->body, ['placeholder' => 'Body Text']) }}
		</div>
		{{ Form::hidden('_method', 'put') }}
		{{ Form::submit('Submit')}}
	{!! Form::close() !!}
@endsection