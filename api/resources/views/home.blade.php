@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <hr>
                    <a href="/posts/create">Create Post</a><br />
                    <a href="/posts">All Posts</a>
                    <h3>Your Blog Posts</h3>

                    @if( count($posts) > 0)
                        @foreach( $posts as $post )
                        <div>
                            <h3><a href="/posts/{{$post->id}}">{{ $post->title }}</a></h3>
                            <small>Written on {{ $post->created_at }}</small>
                        </div>
                        @endforeach

                        {{ $posts->links() }}
                    @else
                        <p>No posts found</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
